package com.example.edt11;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {
    private ListView listViewClass;
    static List<Item> list = new ArrayList<>();
    Item item1 = new Item("La Molina", R.drawable.lamolina, "Cerdanya", "Catalunya", 68, 71, 20, 19, 22, 7);
    Item item2 = new Item("Masella", R.drawable.masella, "Cerdanya", "Catalunya", 65, 74, 9, 24, 22, 10);
    Item item3 = new Item("Port Ainé", R.drawable.portaine, "Pallars Sobirà", "Catalunya", 25, 26.7, 6, 4, 11, 4);
    Item item4 = new Item("Vall de Núria", R.drawable.vallnuria, "Ripollès", "Catalunya", 11, 7.6, 3, 3, 3, 2);
    Item item5 = new Item("Vallter 2000", R.drawable.vallter,"Ripollès", "Catalunya",13, 13.23, 3, 4, 6, 0);
    Item item6 = new Item("Espot Esquí", R.drawable.espotesqui, "Espot, Pallars Sobirá", "Catalunya", 22, 25, 2, 10, 6, 4);
    Item item7 = new Item("Port del Comte", R.drawable.portcompte, "Solsonés", "Catalunya", 39, 50, 8, 12, 13, 6);
    Item item8 = new Item("Grandvalira", R.drawable.grandvalira, "Andorra", 139, 210, 24, 55, 41, 19);
    CustomAdapter cA = new CustomAdapter(this, list);

    public static String EXTRA_TEXT_NAME = "com.example.edt11.EXTRA_TEXT_NAME";
    public static String EXTRA_IMAGE = "com.example.edt11.EXTRA_IMAGE";
    public static int EXTRA_POS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);


        listViewClass = findViewById(R.id.listViewClass);
        listViewClass.setAdapter(cA);

        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);
        list.add(item5);
        list.add(item6);
        list.add(item7);
        list.add(item8);




        listViewClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayInfo(position);


            }
        });
    }
    private void displayInfo(int position) {
        Intent i = new Intent(getApplicationContext(), ReservaActivit.class);
        i.putExtra(EXTRA_TEXT_NAME, list.get(position).getNom());
        i.putExtra(EXTRA_IMAGE, list.get(position).getImgItem());
        i.putExtra(String.valueOf(EXTRA_POS), position);
        startActivity(i);
    }



    private class CustomAdapter extends BaseAdapter {
        private Context context;
        private List<Item> items;

        public CustomAdapter(Context context, List<Item> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data, null);
            TextView nom  = view.findViewById(R.id.textName);
            TextView comarca  = view.findViewById(R.id.textComarca);
            TextView comunitat  = view.findViewById(R.id.textComunitat);
            TextView pistes  = view.findViewById(R.id.textPistes);
            TextView km  = view.findViewById(R.id.textKm);
            TextView verd  = view.findViewById(R.id.textVerd);
            TextView blau  = view.findViewById(R.id.textBlau);
            TextView vermell  = view.findViewById(R.id.textVermell);
            TextView negre  = view.findViewById(R.id.textNegre);
            ImageView imgPista = view.findViewById(R.id.imgPista);

            nom.setText(items.get(position).getNom());
            comarca.setText(items.get(position).getComarca());
            comunitat.setText(items.get(position).getComunitat());
            pistes.setText(items.get(position).getNpistes() + " pistes");
            km.setText(items.get(position).getKm() + " Km");
            verd.setText(items.get(position).getVerd() +"");
            blau.setText(items.get(position).getBlau() + "");
            vermell.setText(items.get(position).getVermell() + "");
            negre.setText(items.get(position).getNegre() + "");
            imgPista.setImageResource(items.get(position).getImgItem());
            return view;
        }
    }
}