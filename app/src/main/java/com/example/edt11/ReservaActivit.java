package com.example.edt11;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservaActivit extends AppCompatActivity {
    private Spinner spinner;
    private ImageView imgGran;
    private Switch aSwitch;
    private RadioGroup radioGroup;
    private RadioButton rBmati;
    private RadioButton rBtarda;
    private RadioButton rBdia;
    private Button sendBttn;

    private EditText nom;
    private EditText cognom;
    private TextInputEditText email;
    private TextInputEditText persones;


    private EditText diaEntrada;
    private EditText diaSortida;
    private DatePickerDialog picker;

    final Calendar cldr = Calendar.getInstance();
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);

        imgGran = findViewById(R.id.imgGran);
        aSwitch = findViewById(R.id.switchForfait);
        radioGroup = findViewById(R.id.rGroup);
        rBmati = findViewById(R.id.rBmati);
        rBtarda = findViewById(R.id.rBtarda);
        rBdia = findViewById(R.id.rBdia);
        diaEntrada = findViewById(R.id.diaEntrada);
        diaSortida = findViewById(R.id.diaSortida);
        sendBttn = findViewById(R.id.sendBttn);

        nom = findViewById(R.id.textName);
        cognom = findViewById(R.id.textCognom);
        email = findViewById(R.id.textEmail);
        persones = findViewById(R.id.textNPersones);

        spinner = findViewById(R.id.spinner);
        List<String> pistes = new ArrayList<>();
        

        //DashboardActivity dashboardActivity = new DashboardActivity();
        for(int i = 0; i < DashboardActivity.list.size(); i++){
            //System.out.println(DashboardActivity.list.get(i).getNom());
            pistes.add(DashboardActivity.list.get(i).getNom());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                pistes
        );


        Intent intent = getIntent();
        String textNom = intent.getStringExtra(DashboardActivity.EXTRA_TEXT_NAME);
        int imgItem = intent.getIntExtra(DashboardActivity.EXTRA_IMAGE, 0);
        int pos = intent.getIntExtra(String.valueOf(DashboardActivity.EXTRA_POS), 0);

        spinner.setAdapter(dataAdapter);
        spinner.setSelection(pos);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ReservaActivit.this, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                imgGran.setImageResource(DashboardActivity.list.get(position).getImgItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imgGran.setImageResource(imgItem);

        if (aSwitch != null){
            aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        radioGroup.setEnabled(true);
                        rBdia.setChecked(true);
                        rBmati.setEnabled(true);
                        rBtarda.setEnabled(true);
                        rBdia.setEnabled(true);

                    } else {
                        radioGroup.clearCheck();
                        rBmati.setEnabled(false);
                        rBtarda.setEnabled(false);
                        rBdia.setEnabled(false);


                    }
                }
            });
        }



        diaEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker = new DatePickerDialog(ReservaActivit.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        diaEntrada.setText(dayOfMonth + "/" + month + "/" + year);
                    }
                }, year, month, day);
                picker.show();
            }
        });

        diaSortida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker = new DatePickerDialog(ReservaActivit.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        diaSortida.setText(dayOfMonth + "/" + month + "/" + year);
                    }
                }, year, month, day);
                picker.show();
            }
        });


        String q = "Nom: " + nom.getText().toString() + "\nCognom: " +cognom.getText().toString()
                + "\nEstació d'esquí: " + textNom.toString() + "\nData entrada: " + diaEntrada.getText().toString()
                + "\nData sortida: " + diaSortida.getText().toString() + "\nEmail: " + email.getText().toString()
                + "\nNum persones: " + persones.getText().toString();


        Intent intentEmail = new Intent(Intent.ACTION_SEND);
        intentEmail.setData(Uri.parse("mailto:"));
        intentEmail.setType("text/plain");
        String[] TO = {"aida.carbonell99@gmail.com"};
        String[] CC = {"acarbonell.bcn@artsgrafiques.org"};

        intentEmail.putExtra(Intent.EXTRA_EMAIL, TO);
        intentEmail.putExtra(Intent.EXTRA_SUBJECT, "patata");
        intentEmail.putExtra(Intent.EXTRA_TEXT, "nom: " + q);


        sendBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(Intent.createChooser(intentEmail, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ReservaActivit.this, "There is no email client installed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });




    }
}