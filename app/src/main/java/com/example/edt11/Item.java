package com.example.edt11;

public class Item {
    private String nom;
    private int imgItem;
    private String comarca;
    private String comunitat;
    private int Npistes;
    private double km;
    private int verd;
    private int blau;
    private int vermell;
    private int negre;

    public Item(String nom, int imgItem, String comarca, String comunitat, int npistes, double km, int verd, int blau, int vermell, int negre) {
        this.nom = nom;
        this.imgItem = imgItem;
        this.comarca = comarca;
        this.comunitat = comunitat;
        this.Npistes = npistes;
        this.km = km;
        this.verd = verd;
        this.blau = blau;
        this.vermell = vermell;
        this.negre = negre;
    }

    public Item(String nom, int imgItem, String comarca, int npistes, double km, int verd, int blau, int vermell, int negre) {
        this.nom = nom;
        this.imgItem = imgItem;
        this.comarca = comarca;
        Npistes = npistes;
        this.km = km;
        this.verd = verd;
        this.blau = blau;
        this.vermell = vermell;
        this.negre = negre;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getImgItem() {
        return imgItem;
    }

    public void setImgItem(int imgItem) {
        this.imgItem = imgItem;
    }

    public String getComarca() {
        return comarca;
    }

    public void setComarca(String comarca) {
        this.comarca = comarca;
    }

    public String getComunitat() {
        return comunitat;
    }

    public void setComunitat(String comunitat) {
        this.comunitat = comunitat;
    }

    public int getNpistes() {
        return Npistes;
    }

    public void setNpistes(int npistes) {
        Npistes = npistes;
    }

    public double getKm() {
        return km;
    }

    public void setKm(double km) {
        this.km = km;
    }

    public int getVerd() {
        return verd;
    }

    public void setVerd(int verd) {
        this.verd = verd;
    }

    public int getBlau() {
        return blau;
    }

    public void setBlau(int blau) {
        this.blau = blau;
    }

    public int getVermell() {
        return vermell;
    }

    public void setVermell(int vermell) {
        this.vermell = vermell;
    }

    public int getNegre() {
        return negre;
    }

    public void setNegre(int negre) {
        this.negre = negre;
    }
}
