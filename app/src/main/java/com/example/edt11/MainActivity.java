package com.example.edt11;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private ImageView imgLogo;
    private ImageView bg1;
    private ImageView bg2;
    private TextView text;

    private ObjectAnimator oA1;
    private ObjectAnimator oA2;
    private ObjectAnimator oA3;
    private ObjectAnimator oA4;
    private ObjectAnimator oA5;
    private ObjectAnimator oA6;
    private ObjectAnimator oA7;
    private ObjectAnimator oA8;
    private ObjectAnimator oA9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgLogo = findViewById(R.id.imgLogo);
        bg1 = findViewById(R.id.bg1);
        bg2 = findViewById(R.id.bg2);
        text = findViewById(R.id.textSkis);

        oA1 = ObjectAnimator.ofFloat(imgLogo, "translationY", 0, 2000f);
        oA1.setDuration(0);
        oA2 = ObjectAnimator.ofFloat(bg1, "translationX", 0, 1000f);
        oA2.setDuration(0);
        oA3 = ObjectAnimator.ofFloat(bg2, "translationX", 0, -1000f);
        oA3.setDuration(0);
        oA4 = ObjectAnimator.ofFloat(text, "translationY", 0, -2000f);
        oA4.setDuration(0);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(oA1, oA2, oA3, oA4);
        set.start();

        oA5 = ObjectAnimator.ofFloat(imgLogo, "translationY",-2000f,  0);
        oA5.setStartDelay(1000);
        oA5.setDuration(1500);
        oA6 = ObjectAnimator.ofFloat(bg1, "translationX",-1000f,  0);
        oA5.setStartDelay(500);
        oA6.setDuration(1000);
        oA7 = ObjectAnimator.ofFloat(bg2, "translationX",1000f,  0);
        oA5.setStartDelay(500);
        oA7.setDuration(1000);
        oA8 = ObjectAnimator.ofFloat(text, "translationY",2000f,  0);
        oA8.setStartDelay(1000);
        oA8.setDuration(1000);

        AnimatorSet set1 = new AnimatorSet();
        set1.playTogether(oA5, oA6, oA7, oA8);
        set1.start();


        set1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View,String>(imgLogo,"imgTrans");
                pairs[1] = new Pair<View,String>(text,"textTrans");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                        MainActivity.this,
                        pairs[0],
                        pairs[1]
                );
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                startActivity(intent, options.toBundle());
            }
        });

    }
}